<?php

/**
 * @file
 * Contains various relationship handlers.
 */

/**
 * Specialized relationship handler associating flag_pages and flag_notes.
 *
 * @ingroup views
 */
class flag_page_handler_relationship_flag_note extends views_handler_relationship {

  function option_definition() {
    $options = parent::option_definition();
    $options['user_scope'] = array('default' => 'current');
    $options['required'] = array('default' => 1);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['user_scope'] = array(
      '#type' => 'radios',
      '#title' => t('By'),
      '#options' => array('current' => t('Current user'), 'any' => t('Any user')),
      '#default_value' => $this->options['user_scope'],
    );

    $form['required']['#title'] = t('Include only content with flag notes');
    $form['required']['#description'] = t('If checked, only content that has a note will be included. To limit to a specific flag use the <em>Flags: Page flag</em> relationship.');
  }

  function ui_name() {
    // We put the bookmark name in the UI string to save space.
    return t('!group: flag notes', array('!group' => $this->definition['group']));
  }

  /**
   * Called to implement a relationship in a query.
   */
  function query() {
    if ($this->options['user_scope'] == 'current') {
      $this->definition['extra'][] = array(
        'field' => 'uid',
        'value' => '***CURRENT_USER***',
        'numeric' => TRUE,
      );
    }
    parent::query();
  }
}

