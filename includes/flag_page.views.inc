<?php
/**
 * @file
 * Integrates flag_page with views
 */
function flag_page_views_data() {
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data = array();

  $data['flag_page_data_url']['table']['group']  = t('Flag page');
  $data['flag_page_data_url']['table']['base'] = array(
    'field' => 'content_id',
    'title' => t('Flag pages'),
    'help' => t('Lists url\'s flagged by Flag pages'),
  );

  $data['flag_page_data_url']['table']['join'] = array(
    // flag_page_data links to flags via content id
    'flag_content' => array(
      'left_field' => 'content_id',
      'field' => 'content_id',
    ),
  );

  $data['flag_page_data_url']['content_id'] = array(
    'title' => t('Content ID'),
    'field' => array(
      'help' => t('Content ID of the flagged page.'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['flag_page_data_url']['url'] = array(
    'title' => t('URL'), // The item it appears as on the UI,
    'field' => array(
      'help' => t('Name of the URL of the flagged page.'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['flag_page_data_url']['title'] = array(
    'title' => t('Link title'), // The item it appears as on the UI,
    'field' => array(
      'help' => t('The title of the flagged page'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['flag_page_data_url']['flag_page_link'] = array(
    'field' => array(
      'title' => t('Flag page link'),
      'help' => t('Formatted link to flagged page'),
      'handler' => 'views_handler_field_flag_page_link',
    ),
  );

  $data['flag_page_data_user']['table']['group']  = t('Flag page');

  $data['flag_page_data_user']['table']['join'] = array(
    // flag_page_data_user links to flag_page_data_url via content id
    'flag_page_data_url' => array(
      'left_field' => 'content_id',
      'field' => 'content_id',
    ),
    'flag_content' => array(
      'left_field' => 'content_id',
      'field' => 'content_id',
    ),
  );

  $data['flag_page_data_user']['title'] = array(
    'title' => t('User defined title'), // The item it appears as on the UI,
    'field' => array(
      'help' => t('The title of the flagged page'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );


  if (module_exists('flag_note')) {
    // Integrate with flag_note module.
    $data['flag_page_data_url']['flag_note_page_rel'] = array(
      'title' => t('Notes on pages'),
      'help' => t('All notes associated with a flagged page.'),
      'relationship' => array(
        'label' => t('Flag note on page'),
        'base' => 'flag_note',
        'base field' => 'content_id',
        'relationship field' => 'content_id',
        'handler' => 'flag_page_handler_relationship_flag_note',
        'flag_type' => 'page',
      ),
    );
  }

  return $data;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function flag_page_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'flag_page') . '/includes',
    ),
    'handlers' => array(
      'views_handler_field_flag_page_link' => array(
        'parent' => 'views_handler_field',
      ),
      'flag_page_handler_relationship_flag_note' => array(
        'parent' => 'views_handler_relationship',
        'file' => 'flag_page_handler_relationships.inc',
      ),
    ),
  );
}
