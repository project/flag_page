<?php
/**
 * @file
 * Field handler to present a flag page link
 */
class views_handler_field_flag_page_link extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['url'] = 'url';
    $this->additional_fields['title'] = 'title';
  }


  function render($values) {
    $url = $values->{$this->aliases['url']};
    // If the flag has user overridden titles use this
    if (isset($values->{$this->aliases['user_title']})) {
      $title = check_plain($values->{$this->aliases['user_title']});
    }
    else {
      $title = check_plain($values->{$this->aliases['title']});
    }
    return l($title, $url, array('class' => 'flag_page_link'));
  }

  function query() {
    // If the user has used the flag_content relationship then we can add in
    // the flag_page_data_user
    if (isset($this->view->relationship['flag_content_rel'])) {
      $flag = flag_get_flag($this->view->relationship['flag_content_rel']->options['flag']);
      if ($flag->override_page_title) {
        // Add the user defined title
        $field = array(
          'user_title' => array(
            'table' => 'flag_page_data_user',
            'field' => 'title',
          ),
        );
        $this->add_additional_fields($field);

        //Copy the extra to complete the join on fid and uid
        $flag_content_alias = $this->view->relationship['flag_content_rel']->alias;
        $this->query->table_queue['flag_page_data_user']['join']->extra = $this->query->table_queue[$flag_content_alias]['join']->extra;
      }
    }
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
}
