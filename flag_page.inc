<?php
/**
 * @file
 * Implements a page flag.
 */

class flag_page extends flag_flag {

  function options() {
    $options = parent::options();
    $options += array(
      'override_page_title' => FALSE,
    );
    return $options;
  }

  function options_form(&$form) {
    parent::options_form($form);
    $form['display']['override_page_title'] = array(
      '#type' => 'checkbox',
      '#title' => t('Override the page title when flagging page'),
      '#default_value' => $this->override_page_title,
      '#access' => empty($this->locked['override_page_title']),
    );

    //remove node type setting as irrelevant
    unset($form['access']['types']);
  }

  /**
   * Deletes a page flag from the database.
   */
  function delete() {
    parent::delete();
    db_delete('flag_page_data_user')
      ->condition('fid', $this->fid)
      ->execute();
    db_delete('block')
      ->condition('module', 'flag_page')
      ->condition('delta', $this->name)
      ->execute();
  }

  /**
   * Validates a flag settings
   */
  function validate() {
    $errors = parent::validate();
    return array_merge_recursive($errors, $this->validate_override_page_title());
  }

  function validate_override_page_title() {
    $errors = array();
    if ($this->override_page_title) {
      // Ensure that the display is set to confirmation form.
      if ($this->link_type != 'confirm') {
        $errors['link_type'][] = array(
          'error' => 'flag_page_override_page_title',
          'message' => t('If you want to override the page title you must use the confirmation link type.'),
        );
      }
    }
    return $errors;
  }

  function _load_content($content_id) {
    $content[$content_id] = array(
      'content_id' => $content_id,
    );
    $data = db_query("SELECT url, title FROM {flag_page_data_url} WHERE content_id = :content_id", array(':content_id' => $content_id))->fetchObject();
    if (isset($data->url)) {
      $content[$content_id] = array(
        'url' => $data->url,
        'title' => $data->title,
      );
    }
    return $content;
  }

  function applies_to_content_object($content_id) {
    return TRUE;
  }

  function get_url_from_content_id($content_id) {
    return db_query("SELECT url FROM {flag_page_data_url} WHERE content_id = :content_id", array(':content_id' => $content_id))->fetchField();
  }

  function get_content_id($url) {
    // Create hash or url for keyed reading of flag_page_data
    $url_hash = sha1($url);
    $content_id = db_query("SELECT content_id FROM {flag_page_data_url} WHERE url_hash = :url_hash AND url = :url", array(':url_hash' => $url_hash, ':url' => $url))->fetchField();

    if (!$content_id) {
      //Set the content id to -1 to indicate a new url
      $content_id = -1;
    }
    return $content_id;
  }

  function check_flag_page_data_user($content_id, $uid, $fid) {
    return (bool) db_query_range("SELECT count(content_id) FROM {flag_page_data_user} WHERE content_id = :content_id AND uid = :uid and fid = :fid", 0, 1, array(':content_id' => $content_id, ':uid' => $uid, ':fid' => $fid))->fetchField();
  }

  function flag($action, &$content_id, $account = NULL, $skip_permission_check = FALSE) {
    // Only create a content_id for a url when a user creates the flag
    if ($content_id == -1) {
      $url = $_GET['destination'];
      if ($url) {
        $content_id = $this->flag_page_create_content_id($url, $_REQUEST['title']);
      }
    }
    return parent::flag($action, $content_id, $account, $skip_permission_check);
  }

  /**
   * @todo Please document this function.
   * @see http://drupal.org/node/1354
   */
  function flag_page_create_content_id($url, $title) {
    // Remove newlines from the URL to avoid header injection attacks.
    $url = str_replace(array("\n", "\r"), '', urldecode($url));

    $flag_page_data = array(
      'url' => $url,
      'url_hash' => sha1($url),
      'title' => $_REQUEST['title'],
    );
    drupal_write_record('flag_page_data_url', $flag_page_data);
    return $flag_page_data['content_id'];
  }

  function get_flag_action($content_id) {
    $flag_action = parent::get_flag_action($content_id);
    $page = $this->fetch_content($content_id);
    $flag_action->content_title = $page->title;
    $flag_action->content_url = _flag_url($page->url);
    return $flag_action;
  }

  function rules_get_event_arguments_definition() {
    return array(
      'term' => array(
        'type' => 'page',
        'label' => t('flagged page'),
        'handler' => 'flag_rules_get_event_argument',
      ),
    );
  }

  function rules_get_element_argument_definition() {
    return array('type' => 'page', 'label' => t('Flagged page'));
  }

  function get_views_info() {
    return array(
      'views table' => 'flag_page_data_url',
      'join field' => 'content_id',
      'title field' => 'description',
      'title' => t('Page flag'),
      'help' => t('Limit results to only those urls flagged by a certain flag; Or display information about the flag set on a term.'),
      'counter title' => t('Page flag counter'),
      'counter help' => t('Include this to gain access to the flag counter field.'),
    );
  }

  function applies_to_content_id_array($content_ids) {
    $passed = array();
    foreach ($content_ids as $content_id) {
      if ($this->applies_to_content_id($content_id)) {
        $passed[$content_id] = TRUE;
      }
    }
    return $passed;
  }

  /*
   * Override get_link_type() function so flag_page module can create a
   * content_id only when necessary.
   */
  function get_link_type() {
    $link_types = flag_get_link_types();

    // Override the default flag_flag_link by changing the module of ther selected link type.
    if (isset($this->link_type) && isset($link_types[$this->link_type])) {
      $link_types[$this->link_type]['module'] = 'flag_page';
      return $link_types[$this->link_type];
    }
    else {
      $link_types['normal']['module'] = 'flag_page';
      return $link_types['normal'];
    }
  }
}
